﻿using OMEX_Business.Models;
using WebServiceReference;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OMEX_Business.Services
{
    public class Core
    {
        private WebService1SoapClient webNet;

        public Core()
        {
            webNet = new WebService1SoapClient(WebService1SoapClient.EndpointConfiguration.WebService1Soap);
        }

        public RespuestaGenericaResponse GetOrigenes(BodyOrigenes body)
        {
            RespuestaGenericaResponse response = new RespuestaGenericaResponse();
            List<OficinasResponse> oficinas = new List<OficinasResponse>();
            string origenes = string.Empty;

            try
            {
                origenes = webNet.getOficinasAsync(EmpresaSolicita: body.EmpresaSolicita)
                    .Result.Body.getOficinasResult;
                if (origenes.Substring(0, 5) == "Error")
                    response = new RespuestaGenericaResponse
                    {
                        body = oficinas,
                        message = origenes.Split(':')[1].Trim(),
                        success = false
                    };
                else
                {
                    foreach (string origen in origenes.Replace("|Sin Error", "").Trim().Split('|'))
                    {
                        if (origen != string.Empty)
                        {
                            oficinas.Add(new OficinasResponse
                            {
                                clave = origen.Split('-')[0].Trim(),
                                nombre = origen.Split('-')[1].Trim()
                            });
                        }
                    }
                    response = new RespuestaGenericaResponse
                    {
                        body = oficinas,
                        message = "Sin Error",
                        success = true
                    };
                }
            }
            catch (Exception e)
            {
                response = new RespuestaGenericaResponse
                {
                    body = oficinas,
                    message = e.Message,
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse GetDestinos(BodyDestinos body)
        {
            RespuestaGenericaResponse response = new RespuestaGenericaResponse();
            List<OficinasResponse> oficinas = new List<OficinasResponse>();
            string destinos = string.Empty;

            try
            {
                destinos = webNet.DestinosIntAsync(Origen: body.Origen, EmpresaSolicita: body.EmpresaSolicita)
                    .Result.Body.DestinosIntResult;

                if (destinos.Substring(0, 5) == "Error")
                    response = new RespuestaGenericaResponse
                    {
                        body = oficinas,
                        message = destinos.Split(':')[1].Trim(),
                        success = false
                    };
                else
                {
                    foreach(string destino in destinos.Replace("|Sin error", "").Trim().Split('|'))
                    {
                        if(destino != string.Empty)
                        {
                            oficinas.Add(new OficinasResponse
                            {
                                clave = destino.Split('-')[0].Trim(),
                                nombre = destino.Split('-')[1].Trim()
                            });
                        }
                    }
                    response = new RespuestaGenericaResponse
                    {
                        body = oficinas,
                        message = "Sin Error",
                        success = true
                    };
                }
            }
            catch (Exception e)
            {
                response = new RespuestaGenericaResponse
                {
                    body = oficinas,
                    message = e.Message,
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse GetCorridas(BodyCorridas body)
        {
            XmlDocument doc = new XmlDocument();

            RespuestaGenericaResponse response = new RespuestaGenericaResponse();
            string corridasStr = string.Empty;
            List<CorridaResponse> corridasList = new List<CorridaResponse>();
            JArray corridasArray = new JArray();

            try
            {
                corridasStr = webNet.ConsultaCorridaAsync(origen: body.Origen, destino: body.Destino,
                    fecha: body.FechaSalida.ToString("ddMMyyyy"), hora: "000001", servicio: 0,
                    empasol: body.EmpresaSolicita, adulto: body.Adulto, insen: body.Insen, nino: body.Nino,
                    maestro: body.Maestro, estudiante: body.Estudiante, horafin: "235959", consecutivo: 0,
                    actpas: 0)
                    .Result.Body.ConsultaCorridaResult;

                corridasStr = "<corridasinter>" + corridasStr
                    .Remove(0, 158)
                    .Replace("<!$MG_CAODBO>", "");

                doc.LoadXml(corridasStr);

                JObject jsonCorridasRaw = JObject.Parse(JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.Indented));
                try
                {
                    corridasArray = (JArray)jsonCorridasRaw["corridasinter"]["Record"];
                }
                catch (Exception)
                {
                    int num_error = 0;
                    var bool_error = int.TryParse(jsonCorridasRaw["corridasinter"]["Record"]["Error_Numero"].ToString(), out num_error);
                    if (num_error != 0 & bool_error)
                        return new RespuestaGenericaResponse
                        {
                            body = corridasList,
                            message = jsonCorridasRaw["corridasinter"]["Record"]["Error_Cadena"].ToString().Trim(),
                            success = false
                        };
                    else
                        corridasArray.Add((JObject)jsonCorridasRaw["corridasinter"]["Record"]);
                }

                foreach(JObject item in corridasArray)
                {
                    corridasList.Add(new CorridaResponse
                    {
                        Corridas = item["Corridas"].ToString().Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList(),
                        CadenaFechaPuntos = item["CadenaFechaPuntos"].ToString().Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                        CadenaHoraOrigenDestino = item["CadenaHoraOrigenDestino"].ToString().Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                        CadenaHorariosPuntos = item["CadenaHorariosPuntos"].ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                        CadenaOficinasPuntos = item["CadenaOficinasPuntos"].ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                        CadenaVersion = item["CadenaVersion"].ToString().Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                        Cupo_Autobus = int.Parse(item["Cupo_Autobus"].ToString()),
                        Destino = item["Destino"].ToString(),
                        Disp_Insen = int.Parse(item["Disp_Insen"].ToString()),
                        Disp_Ninios = int.Parse(item["Disp_Ninios"].ToString()),
                        Empresas_Corrida = item["Empresas_Corrida"].ToString().Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList(),
                        Fecha_Boleto = item["Fecha_Boleto"].ToString(),
                        Fecha_Inicial = item["Fecha_Inicial"].ToString(),
                        Fecha_Llegada = item["Fecha_Llegada"].ToString(),
                        Hora_Llegada = item["Hora_Llegada"].ToString(),
                        NConsecutivoViaje = int.Parse(item["NConsecutivoViaje"].ToString()),
                        Hora_Salida = item["Hora_Salida"].ToString(),
                        NViaje = int.Parse(item["NViaje"].ToString()),
                        Origen = item["Origen"].ToString(),
                        Servicio = item["Servicio"].ToString(),
                        Tarifa = double.Parse(item["Tarifa"].ToString()),
                        Tarifa_Adulto = double.Parse(item["Tarifa_Adulto"].ToString()),
                        Tarifa_Insen = double.Parse(item["Tarifa_Insen"].ToString().Replace("I", "")),
                        Tarifa_Nino = double.Parse(item["Tarifa_Nino"].ToString().Replace("N", "")),
                        Tarifa_Promo = double.Parse(item["Tarifa_Promo"].ToString()),
                        Tipo_Corrida = item["Tipo_Corrida"].ToString(),
                        CadenaOrigenDestinoBol = item["CadenaOrigenDestinoBol"].ToString().Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).ToList()
                    });
                }
                return new RespuestaGenericaResponse
                {
                    body = corridasList,
                    message = string.Empty,
                    success = true
                };
            }
            catch (Exception e)
            {
                response = new RespuestaGenericaResponse
                {
                    body = corridasList,
                    message = $"{e.Message} {e.StackTrace}",
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse GetIntinerario(BodyIntinerario body)
        {
            RespuestaGenericaResponse response = new RespuestaGenericaResponse();
            List<IntinerarioResponse> intinerarios = new List<IntinerarioResponse>();

            XmlDocument document = new XmlDocument();
            JArray intinerarioArray = new JArray();
            int n = 0;

            try
            {
                string intinerarioIntStr = webNet.ConsultaItinerarioIntAsync(Empresas: string.Join('-', body.Empresas),
                    Corridas: string.Join('-', body.Corridas),
                    Fechas: string.Join('-', body.Fechas),
                    EmpresaSolicita: body.EmpresaSolicita)
                    .Result.Body.ConsultaItinerarioIntResult;
                document.LoadXml($"<itinerarioInt>{intinerarioIntStr.Remove(0, 158)}");

                JObject jsonIntinerario = JObject.Parse(JsonConvert.SerializeXmlNode(document, Newtonsoft.Json.Formatting.Indented));

                if (int.Parse(jsonIntinerario["itinerarioInt"]["Error_Numero"].ToString()) != 0)
                {
                    return new RespuestaGenericaResponse
                    {
                        body = intinerarios,
                        message = jsonIntinerario["itinerarioInt"]["Error_Cadena"].ToString(),
                        success = false
                    };
                }

                try
                {
                    intinerarioArray = (JArray)jsonIntinerario["itinerarioInt"]["Record"];
                }
                catch (Exception)
                {
                    intinerarioArray.Add((JArray)jsonIntinerario["itinerarioInt"]["Record"]);
                }

                foreach(var item in intinerarioArray)
                {
                    intinerarios.Add(new IntinerarioResponse
                    {
                        Empresa_Viaja = item["Empresa_Viaja"].ToString(),
                        Fecha_Salida = item["Fecha_Salida"].ToString() == "-"
                        ? intinerarios[n - 1].Fecha_Salida
                        : DateTime.Parse(item["Fecha_Salida"].ToString()),
                        Hora_Salida = item["Hora_Salida"].ToString() == "-"
                        ? intinerarios[n - 1].Hora_Salida
                        : item["Hora_Salida"].ToString(),
                        Oficina = item["Oficina"].ToString()
                    });
                    n++;
                }
                return new RespuestaGenericaResponse
                {
                    body = intinerarios,
                    message = string.Empty,
                    success = true
                };
            }
            catch (Exception e)
            {
                response = new RespuestaGenericaResponse
                {
                    body = intinerarios,
                    message = e.Message,
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse GetDiagramaOcupacion(BodyOcupacion body)
        {
            XmlDocument doc = new();
            RespuestaGenericaResponse response = new();
            OcupacionResponse ocupacion = new()
            {
                Asientos = new List<Asiento>(),
                Filas = 0
            };
            List<Asiento> asientos = new List<Asiento>();

            JArray ocupacionArray = new();
            int i = 0;

            try
            {
                string ocupacionStr = webNet.ConsultaOcupacionDiagramaIntAsync(Consecutivo: body.Consecutivo, Viaje: body.Viaje,
                    Origen: body.Origen, Destino: body.Destino, Empresas: string.Join('-', body.EmpresasLs),
                    Fechas: string.Join('-', body.FechasLs), Corridas: string.Join('-', body.CorridasLs),
                    Version: string.Join('-', body.VersionLs), EmpresaSolicita: body.EmpresaSolicita)
                    .Result.Body.ConsultaOcupacionDiagramaIntResult;

                ocupacionStr = "<DiagramaOcupacionInt>" + ocupacionStr
                    .Remove(0, 172);
                doc.LoadXml(ocupacionStr);

                JObject jsonOcupacionRaw = JObject.Parse(JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.Indented));

                if (int.Parse(jsonOcupacionRaw["DiagramaOcupacionInt"]["Error_Numero"].ToString()) != 0)
                    return new()
                    {
                        body = ocupacion,
                        message = jsonOcupacionRaw["DiagramaOcupacionInt"]["Error_Cadena"].ToString(),
                        success = false
                    };

                List<string> diagrama = jsonOcupacionRaw["DiagramaOcupacionInt"]["Record"]["Diagrama"].ToString().Split(',').ToList();
                foreach(var item in diagrama)
                {
                    asientos.Add(new()
                    {
                        amenidad = item.Trim().Length == 3 ? item.Substring(2, 1) : string.Empty,
                        disponible = jsonOcupacionRaw["DiagramaOcupacionInt"]["Record"]["Ocupacion"].ToString().Substring(i, 1) == "0",
                        numero = item.Substring(0, 2)
                    });
                    i++;
                }

                response = new()
                {
                    body = new OcupacionResponse { Asientos = asientos, Filas = int.Parse(jsonOcupacionRaw["DiagramaOcupacionInt"]["Record"]["Filas"].ToString()) },
                    message = string.Empty,
                    success = true
                };

            }
            catch (Exception e)
            {
                response = new RespuestaGenericaResponse
                {
                    body = ocupacion,
                    message = $"{e.Message} {e.StackTrace}",
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse CrearReservacion(BodyReservacion body)
        {
            RespuestaGenericaResponse response = new();
            ReservacionResponse reservacion = new();

            string nombres = string.Empty;
            string asientos = string.Empty;
            int numero_error = 0, i=1;

            try
            {

                var date = string.Join('-', body.FechaLs.Select(x=>x.ToString("dd/MM/yyyy")));

                foreach(var asiento in body.AsientosLs)
                {
                    if (asiento < 10)
                        asientos += $"{asiento}";
                    else
                        asientos += $"{asiento}";

                    if (i != body.AsientosLs.Count)
                        asientos += ",";
                    i++;
                }
                i = 1;
                foreach (var nombre in body.NombresLs)
                {
                    nombres += nombre.PadRight(35, ' ');
                    if (i != body.AsientosLs.Count)
                        nombres += ",";
                    i++;
                }

                #region PeticionReservacionAsync
                string reservacionStr = webNet
                    .PeticionReservacionAsync(origen: body.Origen, destino: body.Destino,
                    fecha: date, hora: string.Join('-', body.HoraLs),
                    numadulto: body.NumAdulto, numnino: body.NumNino,
                    numinsen: body.NumInsen, corridas: string.Join('-', body.CorridasLs),
                    servicio: body.Servicio, asientos: asientos,
                    nombres: nombres, foliosrespuesta: string.Empty,
                    empresasolicita: body.EmpresaSolicita, empresaviaje: string.Join('-', body.EmpresaViaje),
                    promociones: 0, terminal: "P", cliente: "M",
                    tipooperacion: "CE", viaje: body.Viaje,
                    consecutivo: body.Consecutivo, promoregreso: false)
                    .Result.Body.PeticionReservacionResult;
                #endregion

                if(reservacionStr.Split('|').Count() == 1)
                    return new()
                    {
                        body = reservacion,
                        message = reservacionStr.Trim(),
                        success = false
                    };

                if (reservacionStr.Split('|')[4] == "Error")
                    return new()
                    {
                        body = reservacion,
                        message = reservacionStr.Split('|')[0].Trim(),
                        success = false
                    };

                response = new()
                {
                    body = new ReservacionResponse
                    {
                        Empresas = reservacionStr.Split('|')[2].Split('-').Select(int.Parse).ToList(),
                        FolioReservacion = int.Parse(reservacionStr.Split('|')[0]),
                        PrecioTotal = double.Parse(reservacionStr.Split('|')[1])
                    },
                    message = "Sin Error",
                    success = true
                };
            }
            catch (Exception e)
            {
                response = new()
                {
                    body = reservacion,
                    message = $"{e.Message} {e.StackTrace}",
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse LiberaAsientoReservacion(BodyLiberaReservacion body)
        {
            RespuestaGenericaResponse response = new();

            try
            {
                string liberaAsientoStr = webNet.LiberaReservacionAsync(reservacion: body.Reservacion, empresasolicita: body.EmpresaSolicita, empresaviaje: string.Join('-', body.EmpresaViaje))
                    .Result.Body.LiberaReservacionResult.Trim();

                if (liberaAsientoStr.Substring(0, 6) == "ERROR:")
                {
                    return new()
                    {
                        body = new { },
                        message = liberaAsientoStr,
                        success = false
                    };
                }

                if (liberaAsientoStr.Split('|')[1] == "OK")
                    return new()
                    {
                        body = new { },
                        message = liberaAsientoStr.Split('|')[2],
                        success = true
                    };
                else
                    return new()
                    {
                        body = new { },
                        message = liberaAsientoStr.Split('|')[liberaAsientoStr.Split('|').Count()],
                        success = false
                    };
            }
            catch (Exception e)
            {
                response = new()
                {
                    body = new { },
                    message = $"{e.Message} {e.StackTrace}",
                    success = false
                };
            }

            return response;
        }

        public RespuestaGenericaResponse PeticionConfirmacion(BodyPeticionConfirmacion body)
        {
            RespuestaGenericaResponse response = new();
            ConfirmacionResponse confirmacion = new();

            try
            {
                string peticionConfirmacionStr = webNet.PeticionConfirmacionAsync(empresasolicita: body.EmpresaSolicita, reservacion: body.Reservacion,
                    empresaviaje: string.Join('-', body.EmpresaViaje), consecutivo: body.Consecutivo,
                    viaje: body.Viaje, claveoficinaexterna: 6666, oficinaexterna: string.Empty)
                    .Result.Body.PeticionConfirmacionResult.Trim();

                if (peticionConfirmacionStr.Split('|')[4] == "Error")
                {
                    return new()
                    {
                        body = new { },
                        message = peticionConfirmacionStr.Split('|')[0],
                        success = false
                    };
                }
                if (peticionConfirmacionStr.Split('|')[5] == "Sin Error")
                    return new RespuestaGenericaResponse()
                    {
                        body = new ConfirmacionResponse
                        {
                            NumerosOperacion = peticionConfirmacionStr.Split('|')[0].Split(',').Select(int.Parse).ToList(),
                            PagoTotal = double.Parse(peticionConfirmacionStr.Split('|')[1]),
                            CadenaEmpresa = peticionConfirmacionStr.Split('|')[2].Split('-').Select(int.Parse).ToList(),
                            NIT = int.Parse(peticionConfirmacionStr.Split('|')[3]),
                            PrecioTramos = peticionConfirmacionStr.Split('|')[4].Split('-').Select(double.Parse).ToList()
                        },
                        message = peticionConfirmacionStr.Split('|')[5],
                        success = true
                    };
                else
                    return new RespuestaGenericaResponse()
                    {
                        body = new { },
                        message = peticionConfirmacionStr.Split('|')[peticionConfirmacionStr.Split('|').Count()],
                        success = false
                    };
            }
            catch (Exception e)
            {
                response = new()
                {
                    body = confirmacion,
                    message = $"{e.Message} {e.StackTrace}",
                    success = false
                };
            }

            return response;
        }
    }
}
