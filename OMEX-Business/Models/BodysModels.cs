﻿using System;
namespace OMEX_Business.Models
{
    public class BodyPeticionConfirmacion
    {
        public string? EmpresaSolicita { get; set; }
        public int Reservacion { get; set; }
        public List<int>? EmpresaViaje { get; set; }
        public int Consecutivo { get; set; }
        public int Viaje { get; set; }
    }

    public class BodyLiberaReservacion
    {
        public int Reservacion { get; set; }
        public string? EmpresaSolicita { get; set; }
        public List<int>? EmpresaViaje { get; set; }
    }

    public class BodyReservacion
    {
        public string? Origen { get; set; }
        public string? Destino { get; set; }
        public List<DateTime>? FechaLs { get; set; }
        public List<string>? HoraLs { get; set; }
        public int NumAdulto { get; set; }
        public int NumNino { get; set; }
        public int NumInsen { get; set; }
        public List<int>? CorridasLs { get; set; }
        public string? Servicio { get; set; }
        public List<int>? AsientosLs { get; set; }
        public List<string>? NombresLs { get; set; }
        public string? EmpresaSolicita { get; set; }
        public List<int>? EmpresaViaje { get; set; }
        public int Viaje { get; set; }
        public int Consecutivo { get; set; }
    }

    public class BodyOcupacion
    {
        public int Consecutivo { get; set; }
        public int Viaje { get; set; }
        public string? Origen { get; set; }
        public string? Destino { get; set; }
        public List<int>? EmpresasLs { get; set; }
        public List<string>? FechasLs { get; set; }
        public List<int>? CorridasLs { get; set; }
        public List<string>? VersionLs { get; set; }
        public string? EmpresaSolicita { get; set; }
    }

    public class BodyIntinerario
    {
        public List<int>? Empresas { get; set; }
        public List<int>? Corridas { get; set; }
        public List<string>? Fechas { get; set; }
        public string? EmpresaSolicita { get; set; }
    }

    public class BodyCorridas
    {
        public string? Origen { get; set; }
        public string? Destino { get; set; }
        public DateTime FechaSalida { get; set; }
        public string? EmpresaSolicita { get; set; }
        public int Adulto { get; set; }
        public int Insen { get; set; }
        public int Nino { get; set; }
        public int Maestro { get; set; }
        public int Estudiante { get; set; }
    }

    public class BodyDestinos
    {
        public string? Origen { get; set; }
        public string? EmpresaSolicita { get; set; }
    }

	public class BodyOrigenes
    {
        public string? EmpresaSolicita { get; set; }
    }
}

