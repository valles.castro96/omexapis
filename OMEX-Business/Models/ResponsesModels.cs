﻿using System;
namespace OMEX_Business.Models
{
    #region Peticion Confirmacion
	public class ConfirmacionResponse
    {
		public List<int>? NumerosOperacion { get; set; }
		public double PagoTotal { get; set; }
		public List<int>? CadenaEmpresa { get; set; }
		public int NIT { get; set; }
		public List<double>? PrecioTramos { get; set; }
    }
    #endregion

    #region Peticion Reservacion
    public class ReservacionResponse
    {
		public int FolioReservacion { get; set; }
		public double PrecioTotal { get; set; }
		public List<int>? Empresas { get; set; }
    }
    #endregion

    #region Diagrama Ocupacion
    public class OcupacionResponse
    {
		public List<Asiento>? Asientos { get; set; }
		public int Filas { get; set; }
    }

	public class Asiento
    {
		public string? numero { get; set; }
		public bool disponible { get; set; }
		public string? amenidad { get; set; }
    }
    #endregion

    #region Intinerario
    public class IntinerarioResponse
    {
		public string? Oficina { get; set; }
		public DateTime? Fecha_Salida { get; set; }
		public string? Hora_Salida { get; set; }
		public string? Empresa_Viaja { get; set; }
	}
    #endregion

    #region Corridas
    public class CorridaResponse
    {
		public string? Origen { get; set; }
		public string? Destino { get; set; }
		public string? Fecha_Inicial { get; set; }
		public string? Fecha_Boleto { get; set; }
		public string? Hora_Salida { get; set; }
		public string? Fecha_Llegada { get; set; }
		public string? Hora_Llegada { get; set; }
		public string? Servicio { get; set; }
		public List<int>? Empresas_Corrida { get; set; }
		public double Tarifa { get; set; }
		public double Tarifa_Adulto { get; set; }
		public double Tarifa_Nino { get; set; }
		public double Tarifa_Insen { get; set; }
		public int Cupo_Autobus { get; set; }
		public double Tarifa_Promo { get; set; }
		public List<int>? Corridas { get; set; }
		public List<string>? CadenaOficinasPuntos { get; set; }
		public List<string>? CadenaHoraOrigenDestino { get; set; }
		public List<string>? CadenaFechaPuntos { get; set; }
		public List<string>? CadenaHorariosPuntos { get; set; }
		public List<string>? CadenaVersion { get; set; }
		public int Disp_Ninios { get; set; }
		public int Disp_Insen { get; set; }
		public string? Tipo_Corrida { get; set; }
		public int NViaje { get; set; }
		public int NConsecutivoViaje { get; set; }
		public int Error_Numero { get; set; }
		public string? Error_Cadena { get; set; }
		public List<string>? CadenaOrigenDestinoBol { get; set; }
	}
    #endregion
    #region Oficinas
    public class OficinasResponse
    {
        public string? clave { get; set; }
        public string? nombre { get; set; }
    }
    #endregion
    #region Response Generico
    public class RespuestaGenericaResponse
    {
        public bool success { get; set; }
        public object? body { get; set; }
        public string? message { get; set; }
    }
    #endregion
}

