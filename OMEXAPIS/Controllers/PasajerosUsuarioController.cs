﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using DBModels.DBModels;
using Microsoft.AspNetCore.Mvc;
using OMEXAPIS.DataContext;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OMEXAPIS.Controllers
{
    [Route("api/odm/pasajeros")]
    public class PasajerosUsuarioController : Controller
    {
        private readonly ILogger<PasajerosUsuarioController> _logger;
        private ApplicationDbContext _contextDB;

        public PasajerosUsuarioController(ILogger<PasajerosUsuarioController> logger, ApplicationDbContext contextBD)
        {
            _logger = logger;
            _contextDB = contextBD;
        }

        [HttpGet]
        [Route("get_pasajeros")]
        public JsonResult GetPasajaeros(int id_usuario)
        {
            try
            {
                var pasajeros = _contextDB.PasajerosUsuarios.Where(x => x.id_usuario == id_usuario).ToList();
                if (pasajeros.Count == 0)
                    return Json(new { success = false, message = "Aún no hay pasajeros registrados" });
                return Json(new { success = true, pasajeros = pasajeros });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        [HttpPut]
        [Route("pasajero")]
        public async Task<JsonResult> ActualizarPasajero(int id_pasajero, PasajerosUsuario pasajero_body)
        {
            try
            {
                var pasajero = _contextDB.PasajerosUsuarios.First(x => x.id_pasajero == id_pasajero);
                if (pasajero == null)
                    return Json(new { success = false, message = "El pasajero no existe" });

                pasajero.nombre = pasajero_body.nombre;
                pasajero.apellidos = pasajero_body.apellidos;
                pasajero.avatar = pasajero_body.avatar;
                pasajero.perfil = pasajero_body.perfil;
                pasajero.updated = DateTime.Now;

                _contextDB.PasajerosUsuarios.Update(pasajero);
                await _contextDB.SaveChangesAsync();

                return Json(new { success = true, message = $"Los datos del pasajero {pasajero.nombre} {pasajero.apellidos}, fueron actualizados correctamente" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        [HttpPost]
        [Route("pasajero")]
        public JsonResult CrearPasajero(PasajerosUsuario pasajero_body)
        {
            try
            {
                var completo = _contextDB.PasajerosUsuarios.AddAsync(new PasajerosUsuario
                {
                    apellidos = pasajero_body.apellidos,
                    avatar = pasajero_body.avatar,
                    id_usuario = pasajero_body.id_usuario,
                    nombre = pasajero_body.nombre,
                    perfil = pasajero_body.perfil,
                    updated = DateTime.Now
                });
                if (completo.IsCompleted)
                    return Json(new { success = true, message = $"El pasajero {pasajero_body.nombre} {pasajero_body.apellidos} creado correctamente" });
                else
                    return Json(new { success = false, message = "No se completo el registro del usuario" });
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = e.Message });
            }
        }

        [HttpDelete]
        [Route("pasajero")]
        public async Task<JsonResult> EliminarPasajero(int id_pasajero)
        {
            try
            {
                var pasajero = _contextDB.PasajerosUsuarios.First(x => x.id_pasajero == id_pasajero);
                if (pasajero == null)
                    return Json(new { success = false, message = "El pasajero no existe" });
                _contextDB.PasajerosUsuarios.Remove(pasajero);
                await _contextDB.SaveChangesAsync();
                return Json(new { success = true, message = "Pasajero creado correctamente" });
            }
            catch (Exception e)
            {
                return Json(new { success = true, message = e.Message });
            }
        }
    }
}

