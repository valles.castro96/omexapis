﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OMEXAPIS.DataContext;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OMEXAPIS.Controllers
{
    [Route("api/odm/perfil")]
    public class PerfilController : Controller
    {
        private readonly ILogger<PerfilController> _logger;
        private ApplicationDbContext _contextDB;

        public PerfilController(ILogger<PerfilController> logger, ApplicationDbContext contextBD)
        {
            _logger = logger;
            _contextDB = contextBD;
        }

        [HttpGet]
        [Route("get_perfiles")]
        public JsonResult GetAllPerfiles()
        {
            var perfiles = _contextDB.Perfiles.ToList();
            if (perfiles.Count() > 0)
                return Json(new { success = true, perfiles = perfiles });
            return Json(new { success = false, message = "No existe ningun perfil" });
        }

    }
}

