﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using DBModels.DBModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OMEXAPIS.DataContext;
using OMEXAPIS.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OMEXAPIS.Controllers
{
    [Route("api/odm/usuarios")]
    public class UsuariosController : Controller
    {
        private readonly ILogger<UsuariosController> _logger;
        private ApplicationDbContext _contextDB;

        public UsuariosController(ILogger<UsuariosController> logger, ApplicationDbContext contextBD)
        {
            _logger = logger;
            _contextDB = contextBD;
        }

        [HttpGet]
        [Route("get_usuarios")]
        public JsonResult GetUsuarios()
        {
            var usuarios = _contextDB.Usuarios.ToList();
            if (usuarios.Count == 0)
                return Json(new { success = false, message = "Aun no se han registrado usuarios" });
            return Json(new { success = true, usuarios = usuarios });
        }

        [HttpGet]
        [Route("get_usuario")]
        public JsonResult GetUsuario(int id_usuario)
        {
            var usuario = _contextDB.Usuarios.FirstOrDefault(x => x.id_usuario == id_usuario);
            if (usuario == null)
                return Json(new { success = false, message = "El usuario no existe" });
            return Json(new { success = true, usuario = usuario });
        }

        [HttpPost]
        [Route("get_usuario_facebook")]
        public async Task<JsonResult> GetUsuarioFacebook([FromBody] BodyUsuarioTokenFacebook facebok)
        {
            dynamic response = new ExpandoObject();
            DateTime date = DateTime.Now;
            try
            {
                List<string> tokens = new List<string>();
                Usuario usuario = new Usuario();

                usuario = _contextDB.Usuarios.First(x => x.facebook_token == facebok.body.id
                    || x.email == facebok.body.email);
                if (usuario == null)
                {
                    tokens.Add(facebok.token_firebase);
                    Usuario new_usuario = new Usuario()
                    {
                        apellidos = $"{facebok.body.middle_name} {facebok.body.last_name}",
                        email = facebok.body.email,
                        publicidad = false,
                        facebook_token = facebok.body.id,
                        nombre = facebok.body.first_name,
                        nickname = $"{facebok.body.first_name} {facebok.body.middle_name}",
                        perfil = 0,
                        foto_perfil = facebok.body.picture.data.url,
                        f_nacimiento = facebok.body.birthday,
                        token_firebase = JsonConvert.SerializeObject(tokens),
                        created = date,
                        updated = date
                    };
                    var completo = _contextDB.Usuarios.AddAsync(new_usuario);
                    await _contextDB.SaveChangesAsync();

                    if (completo.IsCompleted)
                        return Json(new { succes = true, usuario = new_usuario });
                    else
                        return Json(new { success = false, message = "No se creo el usaurio" });

                }
                else if (string.IsNullOrEmpty(usuario.facebook_token) || usuario.facebook_token != facebok.body.id)
                {
                    usuario.facebook_token = facebok.body.id;
                    usuario.updated = date;
                    var update = _contextDB.Usuarios.Update(usuario);
                    await _contextDB.SaveChangesAsync();
                }
                if (!tokens.Contains(facebok.token_firebase))
                {
                    usuario.updated = date;
                    tokens.Add(facebok.token_firebase);
                    usuario.token_firebase = JsonConvert.SerializeObject(tokens);
                    var update = _contextDB.Usuarios.Update(usuario);
                    await _contextDB.SaveChangesAsync();
                }
                return Json(new { success = true, usuario = usuario });
            }
            catch (DbEntityValidationException e)
            {
                response.success = false;
                string message = string.Empty;
                foreach (var validation in e.EntityValidationErrors)
                {
                    foreach (var validationError in validation.ValidationErrors)
                    {
                        Trace.TraceInformation($"Property {validationError.PropertyName}, {validationError.ErrorMessage}");
                        message += $"Property {validationError.PropertyName}, {validationError.ErrorMessage}\n";
                    }
                }
                response.message = message;
            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("get_usuario_google")]
        public async Task<JsonResult> GetUsuarioGoogle([FromBody] BodyGoogle google)
        {
            dynamic response = new ExpandoObject();
            DateTime date = DateTime.Now;
            try
            {
                List<string> tokens = new List<string>();

                Usuario usuario = new Usuario();
                usuario = _contextDB.Usuarios.First(x => x.social_token == google.body.Id
                    || x.email == google.body.Email);
                if (usuario != null)
                {
                    tokens.Add(google.token_firebase);
                    Usuario new_usuario = new Usuario()
                    {
                        apellidos = google.body.FamilyName,
                        email = google.body.Email,
                        nickname = google.body.GivenName.Split(' ')[0].ToString() + " "
                            + google.body.FamilyName.Split(' ')[0].ToString(),
                        publicidad = false,
                        social_token = google.body.Id,
                        nombre = google.body.GivenName,
                        foto_perfil = google.body.Picture,
                        perfil = 0,
                        token_firebase = JsonConvert.SerializeObject(tokens),
                        updated = date,
                        created = date
                    };
                    var completo = _contextDB.Usuarios.AddAsync(new_usuario);
                    await _contextDB.SaveChangesAsync();

                    if (completo.IsCompleted)
                        return Json(new { succes = true, usuario = new_usuario });
                    else
                        return Json(new { success = false, message = "No se creo el usaurio" });
                }
                else if (string.IsNullOrEmpty(usuario.social_token) || usuario.social_token == google.body.Id)
                {
                    usuario.social_token = google.body.Id;
                    usuario.updated = date;
                    _contextDB.Usuarios.Update(usuario);
                    await _contextDB.SaveChangesAsync();
                }

                if (!tokens.Contains(google.token_firebase))
                {
                    usuario.updated = date;
                    tokens.Add(google.token_firebase);
                    usuario.token_firebase = JsonConvert.SerializeObject(tokens);
                    _contextDB.Usuarios.Update(usuario);
                    await _contextDB.SaveChangesAsync();
                }

                return Json(new { success = true, usuario = usuario });
            }
            catch (DbEntityValidationException e)
            {
                response.success = false;
                string message = string.Empty;
                foreach (var validation in e.EntityValidationErrors)
                {
                    foreach (var validationError in validation.ValidationErrors)
                    {
                        Trace.TraceInformation($"Property {validationError.PropertyName}, {validationError.ErrorMessage}");
                        message += $"Property {validationError.PropertyName}, {validationError.ErrorMessage}\n";
                    }
                }
                response.message = message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("create_usuario")]
        public async Task<JsonResult> CrearUsuario(IFormCollection request)
        {
            dynamic response = new ExpandoObject();
            DateTime date = DateTime.Now;
            try
            {
                var email = request["email"].ToString();
                string url = string.Empty;
                if (_contextDB.Usuarios.FirstOrDefault(x => x.email == email) == null)
                {
                    #region Imagen
                    if (request.Files.Count > 0)
                    {
                        if (request.Files[0] != null)
                        {
                            var file = request.Files[0];
                            var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
                            var file_name = request["nickname"].ToString().Replace(" ", "").Trim()
                                + "-"
                                + DateTime.Now.ToString("hhmmss")
                                + ext;
                            var path = Path.Combine(Directory.GetCurrentDirectory(), "fotos_usuarios", file_name);
                            url = $"http://appmovil.odm.com.mx/odm-api/fotos_usuarios/" + file_name;
                            using (Stream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
                            {
                                file.CopyTo(fileStream);
                            }
                        }
                    }
                    #endregion
                    var completo = _contextDB.Usuarios.AddAsync(new Usuario()
                    {
                        activo = true,
                        apellidos = request["apellidos"].ToString().ToUpper(),
                        device_id = request["device_id"].ToString(),
                        email = request["email"].ToString(),
                        facebook_token = request["facebook_token"].ToString(),
                        foto_perfil = string.IsNullOrEmpty(url)
                                ? "http://appmovil.odm.com.mx/odm-api/fotos_usuarios/default-user.png"
                                : url,
                        f_nacimiento = DateTime.Parse(request["f_nacimiento"]),
                        nickname = request["nickname"].ToString().ToUpper(),
                        nombre = request["nombre"].ToString().ToUpper(),
                        password_hash = request["password_hash"].ToString(),
                        perfil = int.Parse(request["perfil"].ToString()),
                        publicidad = bool.Parse(request["publicidad"]),
                        social_token = request["social_token"].ToString(),
                        telefono = request["telefono"].ToString(),
                        token_firebase = request["token_firebase"].ToString() == null
                                ? string.Empty
                                : request["token_firebase"].ToString(),
                        created = date,
                        updated = date
                    });
                    await _contextDB.SaveChangesAsync();

                    if (completo.IsCompleted)
                        return Json(new { succes = true, message = "Usuario creado correctamente" });
                    else
                        return Json(new { success = false, message = "No se creo el usaurio" });
                }
                else
                {
                    response.success = false;
                    response.message = "El correo que intentas registrar ya tiene una cuenta existente";
                }
            }
            catch (DbEntityValidationException e)
            {
                response.succes = false;
                string message = string.Empty;
                foreach (var validation in e.EntityValidationErrors)
                {
                    foreach (var validationError in validation.ValidationErrors)
                    {
                        Trace.TraceInformation($"Property {validationError.PropertyName}, {validationError.ErrorMessage}");
                        message += $"Property {validationError.PropertyName}, {validationError.ErrorMessage}\n";
                    }
                }
                response.message = message;
            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("update_usuario")]
        public async Task<JsonResult> UpdateUsuario(IFormCollection request)
        {
            dynamic response = new ExpandoObject();
            DateTime date = DateTime.Now;
            try
            {
                string url = string.Empty;
                Usuario usuario = new Usuario();
                var id = int.Parse(request["id_usuario"]);
                usuario = _contextDB.Usuarios.First(x => x.id_usuario == id);
                if (usuario != null)
                {
                    #region Imagen
                    if (request.Files.Count > 0)
                    {
                        if (request.Files[0] != null)
                        {
                            var file = request.Files[0];
                            var ext = Path.GetExtension(file.FileName).ToLowerInvariant();
                            var file_name = request["nickname"].ToString().Replace(" ", "").Trim()
                                + "-"
                                + DateTime.Now.ToString("hhmmss")
                                + ext;
                            var path = Path.Combine(Directory.GetCurrentDirectory(), "/fotos_usuarios", file_name);
                            url = $"http://appmovil.odm.com.mx/odm-api/fotos_usuarios/" + file_name;
                            using (Stream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
                            {
                                file.CopyTo(fileStream);
                                _contextDB.Usuarios.Update(usuario);
                                await _contextDB.SaveChangesAsync();
                            }
                        }
                    }
                    #endregion
                    usuario.apellidos = request["apellidos"];
                    usuario.device_id = request["device_id"];
                    usuario.f_nacimiento = DateTime.Parse(request["f_nacimiento"]);
                    usuario.nickname = $"{request["nombre"].ToString().Split(' ')[0]} " +
                        $"{request["apellidos"].ToString().Split(' ')[0]}";
                    usuario.nombre = request["nombre"];
                    usuario.perfil = int.Parse(request["perfil"]);
                    usuario.email = request["email"];
                    usuario.telefono = request["telefono"];
                    usuario.updated = date;
                    _contextDB.Usuarios.Update(usuario);
                    await _contextDB.SaveChangesAsync();

                    return Json(new { success = true, message = "Usuario modificado correctamente", usuario = usuario });
                }

            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("logout")]
        public JsonResult Logout([FromBody] BodyGeneric body)
        {
            dynamic response = new ExpandoObject();
            var usuario = _contextDB.Usuarios.FirstOrDefault(x => x.id_usuario == body.id);
            var tokens = JsonConvert.DeserializeObject<List<string>>(body.token_firebase) != null
                ? JsonConvert.DeserializeObject<List<string>>(body.token_firebase)
                : new List<string>();
            if (tokens.Contains(body.token_firebase))
                tokens.Remove(body.token_firebase);
            usuario.token_firebase = tokens.Count == 0 ? "" : JsonConvert.SerializeObject(tokens);
            _contextDB.Usuarios.Update(usuario);
            _contextDB.SaveChanges();
            return Json(new { success = true });
        }
    }
}

