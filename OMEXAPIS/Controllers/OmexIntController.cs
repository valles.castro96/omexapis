﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OMEXAPIS.DataContext;
using WS = OMEX_Business;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OMEXAPIS.Controllers
{
    [Route("api/sucom_web")]
    public class OmexIntController : Controller
    {
        private readonly WS.Services.Core core = new WS.Services.Core();
        private readonly ILogger<OmexIntController> _logger;
        private ApplicationDbContext _contextDB;

        public OmexIntController(ILogger<OmexIntController> logger, ApplicationDbContext contextBD)
        {
            _logger = logger;
            _contextDB = contextBD;
        }

        [HttpPost]
        [Route("get_origenes")]
        public JsonResult GetOrigenes([FromBody] WS.Models.BodyOrigenes body)
        {
            return Json(core.GetOrigenes(body));
        }

        [HttpPost]
        [Route("get_destinos")]
        public JsonResult GetDestinos([FromBody] WS.Models.BodyDestinos body)
        {
            return Json(core.GetDestinos(body));
        }

        [HttpPost]
        [Route("get_corridas")]
        public JsonResult GetCorridas([FromBody] WS.Models.BodyCorridas body)
        {
            return Json(core.GetCorridas(body));
        }

        [HttpPost]
        [Route("get_intinerario")]
        public JsonResult GetIntinerario([FromBody] WS.Models.BodyIntinerario body)
        {
            return Json(core.GetIntinerario(body));
        }

        [HttpPost]
        [Route("get_diagrama_ocupacion")]
        public JsonResult GetDiagramaOcupacion([FromBody] WS.Models.BodyOcupacion body)
        {
            return Json(core.GetDiagramaOcupacion(body));
        }

        [HttpPost]
        [Route("crear_reservacion")]
        public JsonResult CrearReservacion([FromBody] WS.Models.BodyReservacion body)
        {
            return Json(core.CrearReservacion(body));
        }

        [HttpPost]
        [Route("libera_asiento_reservacion")]
        public JsonResult LiberaAsientoReservacion([FromBody] WS.Models.BodyLiberaReservacion body)
        {
            return Json(core.LiberaAsientoReservacion(body));
        }

        [HttpPost]
        [Route("peticion_confirmacion")]
        public JsonResult PeticionConfirmacion([FromBody] WS.Models.BodyPeticionConfirmacion body)
        {
            return Json(core.PeticionConfirmacion(body));
        }
    }
}

