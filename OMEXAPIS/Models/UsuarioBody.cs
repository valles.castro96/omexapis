﻿using System;
namespace OMEXAPIS.Models
{
    #region BodyGeneric
    public class BodyGeneric
    {
        public int id { get; set; }
        public string token_firebase { get; set; }
    }
    #endregion
    #region Google
    public class BodyGoogle
    {
        public GoogleAuthenticateModel body { get; set; }
        public string token_firebase { get; set; }
    }

    public class GoogleAuthenticateModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
    }
    #endregion

    #region Facebook
    public class BodyUsuarioTokenFacebook
    {
        public FacebookAuthenticateModel body { get; set; }
        public string token_firebase { get; set; }
    }

    public class FacebookAuthenticateModel
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
        public Picture picture { get; set; }
        public string short_name { get; set; }
        public string email { get; set; }
        public DateTime birthday { get; set; }
    }

    public class Data
    {
        public string url { get; set; }
    }

    public class Picture
    {
        public Data data { get; set; }
    }
    #endregion
}

