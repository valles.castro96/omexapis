﻿using System;
using DBModels.DBModels;
using Microsoft.EntityFrameworkCore;

namespace OMEXAPIS.DataContext
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

		public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<PasajerosUsuario> PasajerosUsuarios { get; set; }
        public DbSet<Perfil> Perfiles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("ODMINSBDAPP");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}

