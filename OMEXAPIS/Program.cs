﻿using Microsoft.EntityFrameworkCore;
using OMEXAPIS.DataContext;

var builder = WebApplication.CreateBuilder(args);

#region Conexion SQLServer
var connectionString = builder.Configuration.GetConnectionString("ODMINSBDAPP");
builder.Services.AddDbContext<ApplicationDbContext>(x => x.UseSqlServer(connectionString));
#endregion

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

