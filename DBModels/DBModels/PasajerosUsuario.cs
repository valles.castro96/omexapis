﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBModels.DBModels
{
	[Table("PasajerosUsuario")]
	public class PasajerosUsuario
	{
		[Key]
		[Column("id_pasajero")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id_pasajero { get; set; }

		[Column("nombre")]
		public string? nombre { get; set; }

		[Column("apellidos")]
		public string? apellidos { get; set; }

		[Column("id_usuario")]
		public int id_usuario { get; set; }

		[Column("avatar")]
		public string? avatar { get; set; }

		[Column("perfil")]
		public int perfil { get; set; }

		[Column("created")]
		public DateTime created { get; set; }

		[Column("updated")]
		public DateTime updated { get; set; }
	}
}

