﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBModels.DBModels
{
	[Table("Usuario")]
	public class Usuario
	{

		[Key]
		[Column("id_usuario")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id_usuario { get; set; }

		[Column("nickname")]
		public string? nickname { get; set; }

		[Column("nombre")]
		public string? nombre { get; set; }

		[Column("apellidos")]
		public string? apellidos { get; set; }

		[Column("f_nacimiento")]
		public DateTime f_nacimiento { get; set; }

		[Column("password_hash")]
		public string? password_hash { get; set; }

		[Column("activo")]
		public bool activo { get; set; }

		[Column("device_id")]
		public string? device_id { get; set; }

		[Column("email")]
		public string? email { get; set; }

		[Column("publicidad")]
		public bool publicidad { get; set; }

		[Column("foto_perfil")]
		public string? foto_perfil { get; set; }

		[Column("perfil")]
		public int perfil { get; set; }

		[Column("telefono")]
		public string? telefono { get; set; }

		[Column("social_token")]
		public string? social_token { get; set; }

		[Column("facebook_token")]
		public string? facebook_token { get; set; }

		[Column("token_firebase")]
		public string? token_firebase { get; set; }

		[Column("created")]
		public DateTime created { get; set; }

		[Column("updated")]
		public DateTime updated { get; set; }
	}
}

