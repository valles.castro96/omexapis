﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBModels.DBModels
{
	[Table("Perfil")]
	public class Perfil
	{
		[Key]
		[Column("id_perfil")]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id_perfil { get; set; }

		[Column("perfil")]
		public string? perfil { get; set; }

		[Column("regla")]
		public string? regla { get; set; }

		[Column("activo")]
		public bool activo { get; set; }

		[Column("maximo")]
		public int maximo { get; set; }
	}
}

